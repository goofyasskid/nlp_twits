import pandas as pd
import pymorphy2
import collections
import csv
import re
from gensim.models.ldamulticore import LdaMulticore
from gensim.corpora.dictionary import Dictionary
import pyLDAvis.gensim_models as gensimvis
import pyLDAvis


df = pd.read_csv("short_most_recent_words.csv.gz", compression="gzip")  # read original csv file
stop_words_file = open('stopwords-ru.txt', encoding='UTF-8')  # open and read stopwords-ru.txt
stop_words = stop_words_file.read()
morph = pymorphy2.MorphAnalyzer()


def lemmatize(text, count_words_dict):
    words = text.split()
    result = list()
    for word in words:  # break sentences apart
        match = re.search(r'[-+]?\d+', word)
        if word.lower() not in stop_words:  # check if word is stop-word
            if match is None:  # check if word contains digits only
                p = morph.parse(word)[0]
                result.append(p.normal_form)
                count_words_dict[word] += 1  # add word in dict
    return result


count_words_dict = collections.Counter()  # initialize dict
for i in range(len(df)):  # lemmatize for every row in original csv file
    lemmatize(df['text'][i], count_words_dict)

stop_words_file.close()  # close stopwords-ru.txt file

for j in sorted(count_words_dict.elements()):  # check if number of repeat of word < 5
    if count_words_dict[j] < 5:
        del count_words_dict[j]

sorted_tuples = count_words_dict.most_common()
count_words_dict = {k: v for k, v in sorted_tuples}


with open('most_recent_words.csv', 'w', newline='', encoding='utf-8') as csv_file:  # create final csv file
    fieldnames = ['word', 'count']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

    writer.writeheader()
    for key in count_words_dict:
        writer.writerow({'word': key, 'count': count_words_dict[key]})


if __name__ == '__main__':  # create topics

    keys = list(count_words_dict.keys())
    keys = [i.split() for i in keys]
    dictionary = Dictionary(keys)
    corpus = [dictionary.doc2bow(doc) for doc in keys]
    lda_model = LdaMulticore(corpus=corpus, id2word=dictionary, num_topics=20)

    with open(f'topics_{lda_model.num_topics}.txt', mode='w', encoding='utf-8') as file:
        for topic in lda_model.print_topic():
            file.write(str(topic) + '\n')


    # 4th par.
    '''
    pyLDAvis.enable_notebook()
    vis_data = gensimvis.prepare(lda_model, corpus, dictionary)
    pyLDAvis.show(vis_data)
    '''